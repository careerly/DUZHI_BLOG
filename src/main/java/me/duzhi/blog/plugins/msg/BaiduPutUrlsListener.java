package me.duzhi.blog.plugins.msg;

import com.jfinal.kit.LogKit;
import com.jfinal.kit.StrKit;
import io.jpress.message.Message;
import io.jpress.message.MessageListener;
import io.jpress.message.annotation.Listener;
import io.jpress.model.Content;
import io.jpress.model.query.OptionQuery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author ashang.peng@aliyun.com
 * @date 二月 02, 2017
 */
@Listener(action = {Content.ACTION_ADD})
public class BaiduPutUrlsListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        String baidu_site=OptionQuery.me().findValue("_dz_baidu_site");
        if(StrKit.isBlank(baidu_site)){
            return;
        }
        String baidu_token=OptionQuery.me().findValue("_dz_baidu_token");
        if(StrKit.isBlank(baidu_token)){
            return;
        }
        Content content = message.getData();
        try {
           String result =  Post("http://data.zz.baidu.com/urls?site="+baidu_site+"&token="+baidu_token+"&type=original", new String[]{OptionQuery.me().findValue("web_domain")+content.getUrl()});
            LogKit.warn("put result:"+result);
        } catch (Exception e) {
            LogKit.error("put url {" + content.getUrl() + "} error" + e.getMessage());
        }
    }

    /**
     * 百度链接实时推送
     *
     * @param PostUrl
     * @param Parameters
     * @return
     */
    public static String Post(String PostUrl, String[] Parameters) throws IOException {
        if (null == PostUrl || null == Parameters || Parameters.length == 0) {
            return null;
        }
        String result = "";
        PrintWriter out = null;
        BufferedReader in = null;
        try {
            //建立URL之间的连接
            URLConnection conn = new URL(PostUrl).openConnection();
            //设置通用的请求属性
            conn.setRequestProperty("Host", "data.zz.baidu.com");
            conn.setRequestProperty("User-Agent", "curl/7.12.1");
            conn.setRequestProperty("Content-Length", "83");
            conn.setRequestProperty("Content-Type", "text/plain");

            //发送POST请求必须设置如下两行
            conn.setDoInput(true);
            conn.setDoOutput(true);

            //获取conn对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            //发送请求参数
            String param = "";
            for (String s : Parameters) {
                param += s + "\n";
            }
            out.print(param.trim());
            //进行输出流的缓冲
            out.flush();
            //通过BufferedReader输入流来读取Url的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }

        } catch (Exception e) {
            throw  e;
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }
}
