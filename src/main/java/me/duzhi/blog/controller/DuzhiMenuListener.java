package me.duzhi.blog.controller;

import io.jpress.menu.MenuGroup;
import io.jpress.menu.MenuItem;
import io.jpress.menu.MenuManager;
import io.jpress.message.Message;
import io.jpress.message.MessageListener;
import io.jpress.message.annotation.Listener;

/**
 * @author ashang.peng@aliyun.com
 * @date 二月 18, 2017
 */
@Listener(action = MenuManager.ACTION_INIT_MENU, async = false,weight = 11)
public class DuzhiMenuListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        MenuGroup menuGroup = MenuManager.me().getMenuGroupById("addon");
        MenuItem menuItem = new MenuItem("duzhi_config", "/admin/caddon", "插件配置");
        menuGroup.addMenuItem(menuItem);
    }
}
